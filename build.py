# Copyright 2019 Seth VanHeulen
#
# This file is part of lockpick.
#
# lockpick is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lockpick is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with lockpick.  If not, see <https://www.gnu.org/licenses/>.

# pylint: disable=missing-docstring,trailing-newlines

import os
import pathlib
import sys
import xml.etree.ElementTree as xml

def build_package(package_path):
    package = xml.parse(package_path.joinpath('manifest.xml')).getroot()
    files = xml.SubElement(package, 'files')
    for parent_name, _folder_names, file_names in os.walk(package_path):
        for file_name in file_names:
            file_path = pathlib.Path(parent_name, file_name)
            _file = xml.SubElement(files, 'file', size=str(file_path.stat().st_size))
            _file.text = str(file_path.relative_to(package_path.parent))
    return package

def build_packages(package_paths):
    packages = xml.Element('packages')
    for package_path in package_paths:
        package_path = pathlib.Path(package_path).resolve(True)
        packages.append(build_package(package_path))
    return packages

xml.dump(build_packages(sys.argv[1:]))

